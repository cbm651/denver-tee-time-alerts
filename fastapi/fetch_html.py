import requests

# URL of the webpage you want to scrape
url = "https://app.membersports.com/tee-times/3660/4711/0/1/false"

# Send a GET request to the webpage
response = requests.get(url)

# Save the HTML content to a file
with open("webpage.html", "w", encoding="utf-8") as file:
    file.write(response.text)

print("HTML content saved to webpage.html")
