from bs4 import BeautifulSoup
import logging
from datetime import datetime, timedelta
from dotenv import load_dotenv
from playwright.async_api import async_playwright
import smtplib
import os

load_dotenv()  # Load environment variables from .env file

EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_PORT = int(os.getenv('EMAIL_PORT'))
EMAIL_USER = os.getenv('EMAIL_USER')
EMAIL_PASS = os.getenv('EMAIL_PASS')
EMAIL_FROM = os.getenv('EMAIL_FROM')
EMAIL_TO = os.getenv('EMAIL_TO')

logging.basicConfig(level=logging.DEBUG)  # Set logging level to DEBUG for more detailed logs

async def send_email(subject, message):
    try:
        logging.info("Connecting to SMTP server...")
        with smtplib.SMTP(EMAIL_HOST, EMAIL_PORT) as server:
            server.starttls()
            logging.info("Starting TLS...")
            logging.info(f"Logging in as {EMAIL_USER}")
            server.login(EMAIL_USER, EMAIL_PASS)
            email_message = f"Subject: {subject}\n\n{message}"
            logging.info("Preparing email message...")
            logging.info(f"Email message: {email_message}")
            logging.info("Sending email...")
            server.sendmail(EMAIL_FROM, EMAIL_TO, email_message)
            logging.info("Email sent successfully.")
        return True
    except Exception as e:
        logging.error(f"Failed to send email: {e}")
        return False

async def fetch_tee_times_for_date(date):
    url = f"https://app.membersports.com/tee-times/3660/4711/0/1/false?date={date}"
    async with async_playwright() as p:
        browser = await p.chromium.launch(headless=True)
        page = await browser.new_page()
        await page.goto(url)
        await page.wait_for_timeout(5000)  # Wait for 5 seconds to ensure the page is fully loaded

        content = await page.content()
        await browser.close()

    # Save the raw HTML content to a file for debugging
    with open(f'/app/raw_scraped_content_{date}.html', 'w') as file:
        file.write(content)

    # Load the HTML content into BeautifulSoup
    soup = BeautifulSoup(content, 'html.parser')

    # Adjusted selectors based on the provided HTML structure
    available_tee_times = soup.select(".teeTime")
    logging.debug(f"Found {len(available_tee_times)} time slots for {date}.")

    filtered_content = []
    for time_div in available_tee_times:
        logging.debug(f"Processing time div: {time_div}")
        time = time_div.find("div", class_="timeCol").text.strip()

        courses = time_div.select(".availableBookings .courseRow")
        for course in courses:
            name_div = course.select_one(".name")
            if name_div is None:
                logging.error(f"No 'name' div found for time: {time}")
                continue

            name = name_div.text.strip()
            filtered_content.append({"date": date, "time": time, "course": name})
            logging.info(f"Available tee time: {time} at {name} on {date}")

    return filtered_content

async def check_tee_times(alerts=None, start_date=None, end_date=None):
    if start_date is None:
        start_date = datetime.now().strftime('%Y-%m-%d')
    if end_date is None:
        end_date = (datetime.now() + timedelta(days=7)).strftime('%Y-%m-%d')  # Default to one week

    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')

    all_tee_times = []
    current_date = start_date
    while current_date <= end_date:
        date_str = current_date.strftime('%Y-%m-%d')
        tee_times = await fetch_tee_times_for_date(date_str)
        all_tee_times.extend(tee_times)
        current_date += timedelta(days=1)

    with open('/app/scraped_content.html', 'w') as file:
        file.write("\n".join([f"Date: {entry['date']}, Time: {entry['time']}, Course: {entry['course']}" for entry in all_tee_times]))

    if alerts:
        for alert in alerts:
            for tee_time in all_tee_times:
                if (tee_time['date'] == alert.date and
                    alert.start_time <= tee_time['time'] <= alert.end_time and
                    alert.course.lower() in tee_time['course'].lower()):
                    await send_email("Tee Time Alert", f"Available tee time: {tee_time}")

    return all_tee_times

if __name__ == "__main__":
    import asyncio
    asyncio.run(check_tee_times())  # Call the function to scrape tee times for the default range (one week)
