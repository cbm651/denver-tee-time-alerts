from fastapi import FastAPI, BackgroundTasks, Query, HTTPException
from apscheduler.schedulers.background import BackgroundScheduler
from pydantic import BaseModel
from app.scraper import check_tee_times
from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.redis import RedisBackend
from redis import asyncio as aioredis
import smtplib
import os
from dotenv import load_dotenv
import logging
from typing import List, Optional
import asyncio

load_dotenv()  # Load environment variables from .env file

app = FastAPI()

EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_PORT = int(os.getenv('EMAIL_PORT'))
EMAIL_USER = os.getenv('EMAIL_USER')
EMAIL_PASS = os.getenv('EMAIL_PASS')
EMAIL_FROM = os.getenv('EMAIL_FROM')
ENABLE_SCRAPER = os.getenv('ENABLE_SCRAPER') == 'true'
SCRAPER_INTERVAL = int(os.getenv('SCRAPER_INTERVAL', 1))  # Set to 1 minute for testing

logging.basicConfig(level=logging.DEBUG)

alerts = []
scheduler = BackgroundScheduler()

def send_email(recipient, subject, message):
    try:
        logging.info("Connecting to SMTP server...")
        with smtplib.SMTP(EMAIL_HOST, EMAIL_PORT) as server:
            logging.info("Starting TLS...")
            server.starttls()
            logging.info(f"Logging in as {EMAIL_USER}")
            server.login(EMAIL_USER, EMAIL_PASS)
            email_message = f"Subject: {subject}\n\n{message}"
            logging.info(f"Email message: {email_message}")
            logging.info(f"Sending email to {recipient}...")
            server.sendmail(EMAIL_FROM, recipient, email_message)
            logging.info("Email sent successfully.")
        return True
    except smtplib.SMTPAuthenticationError as e:
        logging.error(f"SMTPAuthenticationError: {e.smtp_code} - {e.smtp_error}")
        return False
    except smtplib.SMTPException as e:
        logging.error(f"SMTPException: {e}")
        return False
    except Exception as e:
        logging.error(f"Exception: {e}")
        return False

async def check_tee_times_and_send_alerts():
    logging.info("Running check_tee_times_and_send_alerts...")
    try:
        tee_times = await check_tee_times()
        logging.info(f"Found {len(tee_times)} tee times.")
        for alert in alerts:
            logging.info(f"Checking alert: {alert}")
            matching_tee_times = [
                tt for tt in tee_times if
                (not alert.date or tt['date'] == alert.date) and
                (not alert.start_time or tt['time'] >= alert.start_time) and
                (not alert.end_time or tt['time'] <= alert.end_time) and
                (not alert.course or alert.course.lower() in tt['course'].lower())
            ]
            if matching_tee_times:
                message = "Available tee times:\n" + "\n".join(
                    [f"Date: {tt['date']}, Time: {tt['time']}, Course: {tt['course']}" for tt in matching_tee_times]
                )
                logging.info(f"Sending email to {alert.email} with message: {message}")
                if not send_email(alert.email, "Tee Time Alert", message):
                    logging.error(f"Failed to send email to {alert.email}")
            else:
                logging.info(f"No matching tee times for alert: {alert}")
    except Exception as e:
        logging.error(f"Error in check_tee_times_and_send_alerts: {e}")

def start_scheduler(interval):
    if not scheduler.running:
        scheduler.add_job(lambda: asyncio.run(check_tee_times_and_send_alerts()), 'interval', minutes=interval)
        scheduler.start()
        logging.info(f"Scheduler started with interval: {interval} minutes")
    else:
        logging.info("Scheduler is already running.")

@app.on_event("startup")
async def on_startup():
    redis = aioredis.from_url(f"redis://{os.getenv('REDIS_HOST')}:{os.getenv('REDIS_PORT')}", encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")
    if ENABLE_SCRAPER:
        start_scheduler(SCRAPER_INTERVAL)

class AlertSettings(BaseModel):
    email: str
    interval: int

@app.post("/set-alert/")
def set_alert(settings: AlertSettings, background_tasks: BackgroundTasks):
    logging.info("Setting alert with new settings")
    global EMAIL_TO
    EMAIL_TO = settings.email
    scheduler.remove_all_jobs()
    start_scheduler(settings.interval)
    return {"message": "Alert settings updated"}

class TeeTime(BaseModel):
    date: str
    time: str
    course: str

@app.get("/api/scrape-tee-times", response_model=List[TeeTime])
@cache(expire=60)
async def scrape_tee_times(
    date: Optional[str] = Query(None, description="Date for tee times in YYYY-MM-DD format"),
    startTime: Optional[str] = Query(None, description="Start time for tee times in HH:MM format"),
    endTime: Optional[str] = Query(None, description="End time for tee times in HH:MM format"),
    course: Optional[str] = Query(None, description="Course name for tee times")
):
    logging.info("Scraping tee times...")

    if startTime and endTime and startTime >= endTime:
        raise HTTPException(status_code=400, detail="Start time must be earlier than end time")

    tee_times = await check_tee_times()

    if date:
        tee_times = [tt for tt in tee_times if tt['date'] == date]
    if startTime:
        tee_times = [tt for tt in tee_times if tt['time'] >= startTime]
    if endTime:
        tee_times = [tt for tt in tee_times if tt['time'] <= endTime]
    if course:
        tee_times = [tt for tt in tee_times if course.lower() in tt['course'].lower()]

    return tee_times

class TeeTimeAlert(BaseModel):
    email: str
    interval: int
    date: Optional[str] = None
    start_time: Optional[str] = None
    end_time: Optional[str] = None
    course: Optional[str] = None

@app.post("/api/create-alert")
async def create_alert(alert: TeeTimeAlert):
    alerts.append(alert)
    logging.info(f"Alert created: email={alert.email}, interval={alert.interval}, date={alert.date}, start_time={alert.start_time}, end_time={alert.end_time}, course={alert.course}")
    scheduler.add_job(lambda: asyncio.run(check_tee_times_and_send_alerts()), 'interval', minutes=alert.interval)
    return {"message": "Alert created successfully"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
