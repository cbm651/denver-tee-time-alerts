#!/bin/bash

# Run the curl command to test the endpoint
curl http://localhost:8000/scrape-tee-times/

# Get the container ID or name of the FastAPI service
container_id=$(docker ps --filter "name=denver-tee-time-alerts-api" --format "{{.ID}}")

# Check the logs of the FastAPI container
docker logs $container_id
