import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import TeeTimes from './pages/TeeTimes';
import Alerts from './pages/Alerts';

function App() {
  return (
    <Router>
      <div>
        <h1>Denver Tee Time Alerts</h1>
        <Routes>
          <Route path='/' element={<TeeTimes />} />
          <Route path='/alerts' element={<Alerts />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
