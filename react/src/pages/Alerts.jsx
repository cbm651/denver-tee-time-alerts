import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Alerts = () => {
  const [email, setEmail] = useState('');
  const [interval, setInterval] = useState(60);
  const [date, setDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [course, setCourse] = useState('');
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const fetchCourses = async () => {
      const availableCourses = [
        "Kennedy (West 9 only)",
        "Kennedy Par 3",
        "Harvard Gulch",
        "Wellshire",
        "Willis Case",
        "Evergreen",
        "Overland Park",
        "City Park"
      ];
      setCourses(availableCourses);
    };

    fetchCourses();
  }, []);

  const createAlert = async () => {
    try {
      const alertData = {
        email,
        interval,
        date,
        start_time: startTime,
        end_time: endTime,
        course,
      };

      const response = await axios.post('/api/create-alert', alertData);
      console.log(response.data);
    } catch (error) {
      console.error("Error creating alert:", error);
    }
  };

  return (
    <div>
      <h1>Set Up Tee Time Alert</h1>
      <div>
        <label>
          Email:
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
        <label>
          Date:
          <input
            type="date"
            value={date}
            onChange={(e) => setDate(e.target.value)}
          />
        </label>
        <label>
          Start Time:
          <input
            type="time"
            value={startTime}
            onChange={(e) => setStartTime(e.target.value)}
          />
        </label>
        <label>
          End Time:
          <input
            type="time"
            value={endTime}
            onChange={(e) => setEndTime(e.target.value)}
          />
        </label>
        <label>
          Course:
          <select
            value={course}
            onChange={(e) => setCourse(e.target.value)}
          >
            <option value="">All Courses</option>
            {courses.map((course, index) => (
              <option key={index} value={course}>{course}</option>
            ))}
          </select>
        </label>
        <label>
          Alert Interval (minutes):
          <input
            type="number"
            value={interval}
            onChange={(e) => setInterval(e.target.value)}
          />
        </label>
        <button onClick={createAlert}>
          Create Alert
        </button>
      </div>
      <button onClick={() => window.location.href = '/'}>
        Go to Tee Times
      </button>
    </div>
  );
};

export default Alerts;
