import React, { useState, useEffect } from 'react';
import axios from 'axios';

const TeeTimes = () => {
  const [teeTimes, setTeeTimes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [date, setDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [course, setCourse] = useState('');
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    return () => {
      // Cleanup function to avoid memory leaks
      setTeeTimes([]);
    };
  }, []);

  useEffect(() => {
    // Fetch available courses from API or set them manually
    const fetchCourses = async () => {
      // Example static list of courses, replace with API call if necessary
      const availableCourses = [
        "Kennedy (West 9 only)",
        "Kennedy Par 3",
        "Harvard Gulch",
        "Wellshire",
        "Willis Case",
        "Evergreen",
        "Overland Park",
        "City Park"
      ];
      setCourses(availableCourses);
    };

    fetchCourses();
  }, []);

  const fetchTeeTimes = async () => {
    setLoading(true);
    try {
      const params = {};
      if (date) params.date = date;
      if (startTime) params.startTime = startTime;
      if (endTime) params.endTime = endTime;
      if (course) params.course = course;

      const response = await axios.get('/api/scrape-tee-times', { params });

      // Ensure response is an array
      setTeeTimes(Array.isArray(response.data) ? response.data : []);
    } catch (error) {
      console.error("Error fetching tee times:", error);
      setTeeTimes([]); // Set to empty array on error
    }
    setLoading(false);
  };

  return (
    <div>
      <h1>Available Tee Times</h1>
      <div>
        <label>
          Date:
          <input
            type="date"
            value={date}
            onChange={(e) => setDate(e.target.value)}
          />
        </label>
        <label>
          Start Time:
          <input
            type="time"
            value={startTime}
            onChange={(e) => setStartTime(e.target.value)}
          />
        </label>
        <label>
          End Time:
          <input
            type="time"
            value={endTime}
            onChange={(e) => setEndTime(e.target.value)}
          />
        </label>
        <label>
          Course:
          <select
            value={course}
            onChange={(e) => setCourse(e.target.value)}
          >
            <option value="">All Courses</option>
            {courses.map((course, index) => (
              <option key={index} value={course}>{course}</option>
            ))}
          </select>
        </label>
        <button onClick={fetchTeeTimes} disabled={loading}>
          {loading ? 'Fetching...' : 'Fetch Tee Times'}
        </button>
      </div>
      <ul>
        {teeTimes.map((time, index) => (
          <li key={index}>
            {`Date: ${time.date}, Time: ${time.time}, Course: ${time.course}`}
          </li>
        ))}
      </ul>
      <button onClick={() => window.location.href = '/alerts'}>
        Go to Alerts
      </button>
    </div>
  );
};

export default TeeTimes;
